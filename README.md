# OpenML dataset: efootball-pes2021-all-players-csv

https://www.openml.org/d/43649

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This is a complete set of Player Data from eFootball PES 2021 video game , the latest of the football franchise by KONAMI. It can be useful for generating insights about trends of skills and ratings , as well as comparisons between Leagues and regions.
What's most intriguing is the "scouting" mechanism in the game ; gazillions of agent combinations , leading to different probabilities of acquiring a player , changing by who you already have your squad as well

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43649) of an [OpenML dataset](https://www.openml.org/d/43649). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43649/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43649/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43649/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

